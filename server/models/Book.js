const mongoose = require("mongoose");

const Book = mongoose.Schema({
	Title:String,
	Author:String,
	Image:String,
	Synopsis:String,
	Characters:String,
	fileUrl:String,
	bookType:String,
	bookPrice:String,
	comments:[]
});

mongoose.model("Book", Book);
const express = require('express');
const app = express();
const router = express.Router();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
var cors = require('cors');
require("./models/User");
require("./models/Book");
const path = require("path");
const multer = require("multer");
app.use(express.json());
app.use("/public/uploads/", express.static(__dirname+'/public/uploads/'));
app.use(express.urlencoded({ extended: false }));
app.use(cors());
const port = 3251;

const storage = multer.diskStorage({
    destination: "./public/uploads/",
    filename: function(req, file, cb){
       cb(null,"FILE-" + Date.now() + path.extname(file.originalname));
    }
 });
 
 var upload = multer({ storage: storage });

const UserM = mongoose.model("User");
const BookM = mongoose.model("Book");

const mongoUri = "mongodb://localhost/ktebi?retryWrites=true&w=majority";
mongoose.connect(mongoUri, {useNewUrlParser:true, useUnifiedTopology:true});

mongoose.connection.on("connected", () => {
    console.log("Mongoose connected");
})

mongoose.connection.on("error", (err) => {
    console.log(err);
})

app.get('/', (req,res) => {
    res.send("Ktebi express server");
})

app.post("/signin", (req,res) => {
    const {email,password} = req.body;
    UserM.findOne({"Email":email,"Password":password}).exec((err, user) => {
        if (!user) {
            console.log("401: Wrong pass or email (email trying " + email + " )");
            res.status(401).json({
                error: 'Veuillez saisir le mot de passe ou votre adress mail correctement'
              });
        }
        else {
            res.json(user);
        }
    });
})

app.post("/gsignin", (req,res) => {
    const {Identifiant,Email,Password} = req.body;
    UserM.findOne({"Email":Email}).exec((err, user) => {
        if (!user) {
            const U = new UserM({
                Identifiant,Email,Password
            });
            U.save().then(data =>{
                console.log(data)
                res.send(data);
            }).catch(err => {
                console.log(err)
            }); 
        }
        else {
            res.json(user);
        }
    });
})


app.get("/fetchBooks", (req,res) => {
    BookM.find({}).then(data => {
        console.log(data);
        res.send(data);
    }).catch(err => {
        console.log(err);
    })
})

app.post("/fetchMyBooks", (req,res) => {
    const {_id} = req.body;
    UserM.findOne({"_id":_id}).exec((err, user) => {
        if (!user) {
            console.log("401: user not found (user " + _id + " )");
            res.status(401).json({
                error: 'Utilisateur non trouvé'
              });
        }
        else {
            res.json(user.BookList);
        }
    });
})

app.post('/register', (req,res) => {
    const {Identifiant,Email,Password} = req.body;
    UserM.findOne({"Email":Email}).exec((err, user) => {
        if (user) {
            res.status(401).json({
                error: 'Compte existant'
              });
        }
        else {
            const U = new UserM({
                Identifiant,Email,Password
            });
            U.save().then(data =>{
                console.log(data)
                res.send(data);
            }).catch(err => {
                console.log(err)
            }); 
        }
    });

});

app.post('/addBook', upload.array("bookFiles",2), async(req, res, next) => {
    const files = req.files;
    if(!files)
    {
        const error = new Error('Please upload imagecover and bookfile');
        error.httpStatusCode = 400
      
        return next("Upload image and bookfile");
    }
    console.log(files);
    
    var imageFilePath = files[0].mimetype.includes("image") ? files[0].path : files[1].path;
    var bookFilePath = files[0].mimetype.includes("image") ? files[1].path : files[0].path;
    //const {Title, Author, Image, Synopsis, Characters, fileUrl, bookType} = req.body;
    const {Title, Author, Synopsis, Characters, bookType,bookPrice} = req.body;
    const B = new BookM({
        Title,
        Author,
        Image:imageFilePath.replaceAll('\\', '/'),
        Synopsis,
        Characters,
        fileUrl:bookFilePath.replaceAll('\\', '/'),
        bookType,
        bookPrice
    });
    B.save().then(data =>{
        console.log(data)
        res.status(201).json(data);
    }).catch(err => {
        console.log(err)
    }); 
})

app.post('/hasBook', (req,res) => {
    const {book,userID} = req.body;
    UserM.findOne({"_id":userID}).exec((err, user) => {
        var found = false;
        var list = user.BookList;
        for(var i = 0; i<list.length; i++) {
            if(list[i]._id == book._id)
            {
                found = true;
                break;
            }
        }
        if (found) {
            console.log("401: User already has that book");
            res.status(401).json({
                error: 'You already own this book !'
              });
        } else res.status(200).json(book);
    });
})

app.post('/buyBook', (req,res) => {
    const {book,userID} = req.body;
    UserM.findOne({"_id":userID}).exec((err, user) => {
        var found = false;
        var list = user.BookList;
        for(var i = 0; i<list.length; i++) {
            if(list[i]._id == book._id)
            {
                found = true;
                break;
            }
        }
        if (found) {
            console.log("401: User already has that book");
            res.status(401).json({
                error: 'Vous avez déjà ce livre !'
              });
        }
        else {
            user.BookList.push(book);
            user.save(function(err) {
                if (err) { throw err; }
                else {
                    console.log("User ("+userID+") book list updated");
                }
            });
            res.json(user);
        }
    });
})

app.post('/commentBook', (req,res) => {
    const {bookID,userName,userID,commentaire} = req.body;

    BookM.findById(bookID).exec((err, book) => {
        if (book) {
            var commentBlock = {"userName":userName,"userID":userID,"commentaire":commentaire};
            book.comments.push(commentBlock);
            book.save();
            res.send(book.comments);
        } else res.send("Book not found");
    });
});

app.post('/getBookComments', (req,res) => {
    const {bookID} = req.body;

    BookM.findById(bookID).exec((err, book) => {
        if (book) {
           res.send(book.comments);
        }
    });
});

app.listen(port, () => {
    console.log("Server running at port " + port);
})
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:ktebi/AddBook.dart';
import 'dart:convert';
import 'book_info.dart';
import 'browse.dart';
import 'library.dart';
import 'more.dart';
import 'news/news.dart';
class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _currentIndex = 2;
  final List<Widget> _interfaces = [const Library(),const News(),const Browse(),const AddBook(),const More()];

  final List<String> titles = ["Library","News", "Browse", "Add Book", "More"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle:true,
        title:Text(titles[_currentIndex],style:const TextStyle(color:Color(0xFFefd1a9))),
        backgroundColor: const Color(0xFF2D453F),
        automaticallyImplyLeading: false,
      ),
      body :Stack(
          children:  [
            const Positioned.fill(
              child: Image(
                image: AssetImage('assets/images/background2.png'),
                fit : BoxFit.fill,
              ),
            ),
            _interfaces[_currentIndex],
          ]
      ),
      bottomNavigationBar:Container(
        decoration: BoxDecoration(
          color: const Color(0xFF2D453F),
          border: Border.all(color: const Color(0xFF2D453F),),
        ),
        child: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            currentIndex: _currentIndex,
            onTap: (int index) {
              if(index != 5)
              {
                setState(() {
                  _currentIndex = index;
                });
                print("changing to page : " + titles[_currentIndex]);
              }

            },
            items: const [
              BottomNavigationBarItem(
                icon: Icon(Icons.library_books,color: Color(0xFF2D453F),),
                title: Text("Library",style:TextStyle(color:Color(0xFF2D453F))),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.info,color: Color(0xFF2D453F)),
                title: Text("News",style:TextStyle(color:Color(0xFF2D453F))),
                backgroundColor: Color(0xFFefd1a9),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.auto_stories,color: Color(0xFF2D453F)),
                title: Text("Browse",style:TextStyle(color:Color(0xFF2D453F))),
                backgroundColor: Color(0xFFefd1a9),
              ),
              BottomNavigationBarItem(
                  icon: Icon(Icons.add_box,color: Color(0xFF2D453F)),
                  title: Text("Add Book",style:TextStyle(color:Color(0xFF2D453F)))
              ),
              BottomNavigationBarItem(
                  icon: Icon(Icons.more_horiz,color: Color(0xFF2D453F)),
                  title: Text("More",style:TextStyle(color:Color(0xFF2D453F)))
              ),

            ],
            backgroundColor: const Color(0xFFefd1a9)
        ),
      ),);
  }
}

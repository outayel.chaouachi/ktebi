import 'package:flutter/material.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class BookDetails extends StatefulWidget {
  const BookDetails({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _BookDetailsState();
  }
}

class _BookDetailsState extends State<BookDetails> {
  late String id;
  late String title;
  late String author;
  late String image;
  late String synopsis;
  late String fileurl;
  late String booktype;
  late String bookprice;
  late String userID;

  late SharedPreferences prefs;
  late Future<bool> bookInfo;

  final String _baseUrl = "167.86.121.21:3251";
  Map<String, dynamic>? paymentIntentData;
  Future<bool> getBookInfo() async {
    prefs = await SharedPreferences.getInstance();
    userID = prefs.getString("userId")!;
    id = prefs.getString("bookId")!;
    author = prefs.getString("bookAuthor")!;
    image = prefs.getString("bookImage")!;
    title = prefs.getString("bookTitle")!;
    synopsis = prefs.getString("bookSynopsis")!;
    fileurl = prefs.getString("bookUrl")!;
    booktype = prefs.getString("bookType")!;
    bookprice = prefs.getString("bookPrice")!;
    print(bookprice);
    return true;
  }

  void SendDetailsToPayment() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("bookId",id);
    Navigator.pushNamed(context, "/paymentscreen");
  }

  @override
  void initState() {
    bookInfo = getBookInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: bookInfo,
      builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
        if(snapshot.hasData) {
          return Scaffold(
            appBar: AppBar(
              title: Text(title, style:const TextStyle(color:Color(0xFFefd1a9))),

              backgroundColor: const Color(0xFF2D453F),
            ),
            body: Stack(
              children:  [
                const Positioned.fill(
                  child: Image(
                    image: AssetImage('assets/images/background2.png'),
                    fit : BoxFit.fill,
                  ),
                ),
                Column(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          color: const Color(0xFFefd1a9),
                          image: DecorationImage(
                            image: NetworkImage("http://"+ _baseUrl + "/" + image),
                          ),
                          border: Border.all(
                            color: Color(0xFFefd1a9),
                            width: 3.0,

                          ),
                        ),
                        width: 460, height: 215,
                        margin: const EdgeInsets.fromLTRB(135, 30, 135, 20),

                      ),
                      const Divider(height: 20,thickness: 3,color: Color(0xFFefd1a9),),
                      Container(
                        margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                        child: Text(author,textScaleFactor: 1.5, style:const TextStyle(color:Color(0xFFefd1a9))),
                      ),
                      const Divider(height: 20,thickness: 3,color: Color(0xFFefd1a9),),
                      Container(
                        margin: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                        child: Text(synopsis, style:const TextStyle(color:Color(0xFFefd1a9))),
                      ),
                      const Divider(height: 20,thickness: 3,color: Color(0xFFefd1a9),),
                      Container(
                        margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                        child: ElevatedButton.icon(
                          icon: Icon(Icons.comment,color: Color(0xFF2D453F)),
                          label: const Text("Comments", textScaleFactor: 1.3,style:TextStyle(color:Color(0xFF2D453F))),
                          style:ElevatedButton.styleFrom(primary:const Color(0xFFefd1a9)),
                          onPressed: () {
                            Navigator.pushNamed(context,"/comments");
                          },
                        ),
                      ),
                    ]
                ),
              ],
            ),
            floatingActionButton: FloatingActionButton.extended(
                shape: BeveledRectangleBorder(
                    borderRadius: BorderRadius.circular(3.5)
                ),
                label: const Text("Buy", textScaleFactor: 1.3,style:TextStyle(color:Color(0xFF2D453F))),
                icon: const Icon(Icons.bookmark,color: Color(0xFF2D453F)),
                backgroundColor: const Color(0xFFefd1a9),
                onPressed: () {
                  //SendDetailsToPayment();
                  Map<String, dynamic> bookData = {
                    "_id" : id,
                    "Title" : title,
                    "Author" : author,
                    "Image" : image,
                    "Synopsis" : synopsis,
                    "fileUrl" : fileurl,
                    "bookType" : booktype
                  };

                  Map<String, dynamic> buyData = {
                    "book": bookData,
                    "userID" : userID
                  };

                  Map<String, String> headers = {
                    "Content-Type": "application/json; charset=UTF-8"
                  };

                  http.post(Uri.http(_baseUrl, "/hasBook"), headers: headers, body: json.encode(buyData))
                      .then((http.Response response) async {
                    if(response.statusCode == 200) {
                      if(int.parse(bookprice) > 0)
                        {
                          showDialog(
                              context: context,
                              builder: (BuildContext context2) {
                                return AlertDialog(
                                  title:Text("Payment"),
                                  content:InkWell(
                                    onTap: ()async{
                                      await makePayment();
                                    },
                                    child: Container(
                                      height: 50,
                                      width: 200,
                                      color: Colors.green,
                                      child: Center(
                                        child: Text('Pay' , style: TextStyle(color: Colors.white , fontSize: 20),),
                                      ),
                                    ),
                                  ),
                                );
                              }
                          );
                        } else {
                        Map<String, dynamic> bookData = {
                          "_id" : id,
                          "Title" : title,
                          "Author" : author,
                          "Image" : image,
                          "Synopsis" : synopsis,
                          "fileUrl" : fileurl,
                          "bookType" : booktype
                        };

                        Map<String, dynamic> buyData = {
                          "book": bookData,
                          "userID" : userID
                        };

                        Map<String, String> headers = {
                          "Content-Type": "application/json; charset=UTF-8"
                        };
                        http.post(Uri.http(_baseUrl, "/buyBook"), headers: headers, body: json.encode(buyData))
                            .then((http.Response response) async {
                          if(response.statusCode == 200) {
                            Map<String, dynamic> userFromServer = json.decode(response.body);

                            SharedPreferences prefs = await SharedPreferences.getInstance();
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Text("Information"),
                                    content: Text("Book bought successfully."),
                                    actions: <Widget>[
                                      TextButton(
                                        child: const Text('OK'),
                                        onPressed: () {
                                          Navigator.pushNamed(context,"/home");
                                        },
                                      ),
                                    ],
                                  );
                                });
                            Navigator.pushNamed(context, "/home");
                          }
                          else if(response.statusCode == 401) {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return const AlertDialog(
                                    title: Text("Information"),
                                    content: Text("You already own this book !"),
                                  );
                                });
                          }
                          else {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return const AlertDialog(
                                    title: Text("Information"),
                                    content: Text("An error occured please try again !"),
                                  );
                                });
                          }
                        });
                      }

                    } else {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return const AlertDialog(
                              title: Text("Information"),
                              content: Text("You already own this book !"),
                            );
                          });
                    }

                    });


                  /**/
                }
            ),
          );
        } else {
          return Scaffold(
            appBar: AppBar(
              title: const Text("Loading"),
            ),
            body: const Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
      }
    );
  }
  Future<void> makePayment() async {
    try {

      paymentIntentData =
      await createPaymentIntent(bookprice, 'USD'); //json.decode(response.body);
      // print('Response body==>${response.body.toString()}');
      await Stripe.instance.initPaymentSheet(
          paymentSheetParameters: SetupPaymentSheetParameters(
              paymentIntentClientSecret: paymentIntentData!['client_secret'],
              applePay: true,
              googlePay: true,
              testEnv: true,
              style: ThemeMode.dark,
              merchantCountryCode: 'US',
              merchantDisplayName: 'ANNIE')).then((value){
      });


      ///now finally display payment sheeet
      displayPaymentSheet();
    } catch (e, s) {
      print('exception:$e$s');
    }
  }

  displayPaymentSheet() async {

    try {
      await Stripe.instance.presentPaymentSheet(
          parameters: PresentPaymentSheetParameters(
            clientSecret: paymentIntentData!['client_secret'],
            confirmPayment: true,
          )).then((newValue){


        print('payment intent'+paymentIntentData!['id'].toString());
        print('payment intent'+paymentIntentData!['client_secret'].toString());
        print('payment intent'+paymentIntentData!['amount'].toString());
        print('payment intent'+paymentIntentData.toString());
        //orderPlaceApi(paymentIntentData!['id'].toString());
        print("paid successfully");
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("paid successfully")));
        Map<String, dynamic> bookData = {
          "_id" : id,
          "Title" : title,
          "Author" : author,
          "Image" : image,
          "Synopsis" : synopsis,
          "fileUrl" : fileurl,
          "bookType" : booktype
        };

        Map<String, dynamic> buyData = {
          "book": bookData,
          "userID" : userID
        };

        Map<String, String> headers = {
          "Content-Type": "application/json; charset=UTF-8"
        };
        http.post(Uri.http(_baseUrl, "/buyBook"), headers: headers, body: json.encode(buyData))
            .then((http.Response response) async {
          if(response.statusCode == 200) {
            Map<String, dynamic> userFromServer = json.decode(response.body);

            SharedPreferences prefs = await SharedPreferences.getInstance();
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Text("Information"),
                    content: Text("Book bought successfully."),
                    actions: <Widget>[
                      TextButton(
                        child: const Text('OK'),
                        onPressed: () {
                          Navigator.pushNamed(context,"/home");
                        },
                      ),
                    ],
                  );
                });
            Navigator.pushNamed(context, "/home");
          }
          else if(response.statusCode == 401) {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return const AlertDialog(
                    title: Text("Information"),
                    content: Text("You already own this book !"),
                  );
                });
          }
          else {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return const AlertDialog(
                    title: Text("Information"),
                    content: Text("An error occured please try again !"),
                  );
                });
          }
        });

        paymentIntentData = null;

      }).onError((error, stackTrace){
        print('Exception/DISPLAYPAYMENTSHEET==> $error $stackTrace');
      });


    } on StripeException catch (e) {
      print('Exception/DISPLAYPAYMENTSHEET==> $e');
      showDialog(
          context: context,
          builder: (_) => AlertDialog(
            content: Text("Cancelled "),
          ));
    } catch (e) {
      print('$e');
    }
  }

  //  Future<Map<String, dynamic>>
  createPaymentIntent(String amount, String currency) async {
    try {
      Map<String, dynamic> body = {
        'amount': calculateAmount(bookprice),
        'currency': currency,
        'payment_method_types[]': 'card'
      };
      print(body);
      var response = await http.post(
          Uri.parse('https://api.stripe.com/v1/payment_intents'),
          body: body,
          headers: {
            'Authorization':
            'Bearer sk_test_51LAgX0COqyso6xw7ipz7UCNNczsGgzeU4KziPKTm6zTjjCi4ygNaVqo1J27EfpsYIk1jdxoUFcaNjywJBsIShIPh00pN03loO3',
            'Content-Type': 'application/x-www-form-urlencoded'
          });
      print('Create Intent reponse ===> ${response.body.toString()}');
      return jsonDecode(response.body);
    } catch (err) {
      print('err charging user: ${err.toString()}');
    }
  }

  calculateAmount(String amount) {
    final a = (int.parse(amount)) * 100 ;
    return a.toString();
  }
}
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'book_info.dart';

class Browse extends StatefulWidget {
  const Browse({Key? key}) : super(key: key);

  @override
  _BrowseState createState() => _BrowseState();
}

class _BrowseState extends State<Browse> {
  late Future<bool> fetchedBooks;

  late List<Book> _books = [];

  final String _baseUrl = "167.86.121.21:3251";

  Future<bool> fetchBooks() async {
    http.Response response = await http.get(Uri.http(_baseUrl, "/fetchBooks"));

    List<dynamic> booksFromServer = json.decode(response.body);

    for(int i = 0; i < booksFromServer.length; i++) {
      Map<String, dynamic> bookFromServer = booksFromServer[i];
      _books.add(Book(bookFromServer["_id"], bookFromServer["Title"], bookFromServer["Author"],
          bookFromServer["Image"].replaceAll('\\', '/'), bookFromServer["Synopsis"],bookFromServer["fileUrl"],bookFromServer["bookType"],bookFromServer["bookPrice"]));
    }
    return true;
  }

  @override
  void initState() {
    fetchedBooks = fetchBooks();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      
      future: fetchedBooks,
      builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
        if(snapshot.hasData) {
          return ListView.builder(
            itemCount: _books.length,
            itemBuilder: (BuildContext context,int index) {
              return BookInfo(_books[index].id, _books[index].title, _books[index].author, _books[index].image,
                  _books[index].synopsis, _books[index].fileurl, _books[index].booktype,_books[index].bookprice,false);
            },
          );
        }
        else {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}
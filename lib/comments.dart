import 'dart:convert';

import 'package:comment_box/comment/comment.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class Comments extends StatefulWidget {
  @override
  _CommentsState createState() => _CommentsState();
}

class _CommentsState extends State<Comments> {
  final formKey = GlobalKey<FormState>();
  final TextEditingController commentController = TextEditingController();
  late String bookID;
  late String userID;
  late String identifiant;
  final String _baseUrl = "167.86.121.21:3251";
  late List filedata;
  /*List filedata = [
    {
      'name': 'Test user ',
      'pic': 'https://picsum.photos/300/30',
      'message': 'I love this book'
    },
    {
      'name': 'Another user',
      'pic': 'https://picsum.photos/300/30',
      'message': 'Very cool'
    },
    {
      'name': 'Book addict',
      'pic': 'https://picsum.photos/300/30',
      'message': 'Very cool'
    },
    {
      'name': 'John doe',
      'pic': 'https://picsum.photos/300/30',
      'message': 'Nice book'
    },
  ];*/

  void addComment(String comment)
  {
    Map<String, dynamic> userData = {
      "bookID": bookID,
      "userName":identifiant,
      "userID":userID,
      "commentaire":comment
    };

    Map<String, String> headers = {
      "Content-Type": "application/json; charset=UTF-8"
    };
    http.post(Uri.http(_baseUrl, "/commentBook"), headers: headers, body: json.encode(userData))
        .then((http.Response response) async {
      if(response.statusCode == 200) {
        //commentsFromServer = ;
        AlertDialog(
          title: Text("Information"),
          content: Text("Commentaire ajouté."),
          actions: <Widget>[
            TextButton(
              child: const Text('OK'),
              onPressed: () {

              },
            ),
          ],
        );
      }
    });
  }

  Future<bool> fetchBooks() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userID = prefs.getString("userId")!;
    identifiant = prefs.getString("Identifiant")!;
    bookID = prefs.getString("bookId")!;
    Map<String, dynamic> userData = {
      "bookID": bookID,
    };

    Map<String, String> headers = {
      "Content-Type": "application/json; charset=UTF-8"
    };

    http.post(Uri.http(_baseUrl, "/getBookComments"), headers: headers, body: json.encode(userData))
        .then((http.Response response) async {
      if(response.statusCode == 200) {
        //commentsFromServer = ;
        setState(() => filedata = json.decode(response.body));
      }
    });

    return true;
  }

  Widget commentChild(data) {
    return Container(
      color: Color(0xFF2D453F),
      child: ListView(
        children: [
          for (var i = 0; i < data.length; i++)
            Padding(
              padding: const EdgeInsets.fromLTRB(2.0, 8.0, 2.0, 0.0),
              child: ListTile(
                leading: GestureDetector(
                  onTap: () async {
                    print("Comment Clicked");
                  },
                  child: Container(
                    height: 50.0,
                    width: 50.0,
                    decoration: const BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.all(Radius.circular(50))),
                    child: CircleAvatar(
                        radius: 50,
                        backgroundImage: NetworkImage("https://picsum.photos/300/30" + "$i")),
                  ),
                ),
                title: Text(
                  data[i]['userName'],
                  style: TextStyle(fontWeight: FontWeight.bold,color: Colors.white),
                ),
                subtitle: Text(data[i]['commentaire'],style: TextStyle(fontWeight: FontWeight.bold,color: Colors.white)),
              ),
            )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Comments",style:TextStyle(color:Color(0xFFefd1a9))),
        backgroundColor: const Color(0xFF2D453F),
      ),
      body: Container(
        child: FutureBuilder<bool>(
          future:fetchBooks(),
            builder: (BuildContext context, AsyncSnapshot<bool> snapshot)
            {
              if(snapshot.hasData)
                {
                  return CommentBox(
                    userImage:
                    "https://lh3.googleusercontent.com/a-/AOh14GjRHcaendrf6gU5fPIVd8GIl1OgblrMMvGUoCBj4g=s400",
                    child: commentChild(filedata),
                    labelText: 'Write a comment...',
                    withBorder: false,
                    errorText: 'Comment cannot be blank',
                    sendButtonMethod: () {
                      if (formKey.currentState!.validate()) {
                        print(commentController.text);
                        /*setState(() {
                          var value = {
                            'name': 'Current User',
                            'pic':
                            'https://lh3.googleusercontent.com/a-/AOh14GjRHcaendrf6gU5fPIVd8GIl1OgblrMMvGUoCBj4g=s400',
                            'message': commentController.text
                          };
                          filedata.insert(0, value);
                        });*/
                        addComment(commentController.text);
                        commentController.clear();
                        FocusScope.of(context).unfocus();
                      } else {
                        print("Not validated");
                      }
                    },
                    formKey: formKey,
                    commentController: commentController,
                    backgroundColor: const Color(0xFFefd1a9),
                    textColor: Color(0xFF2D453F),
                    sendWidget: Icon(Icons.send_sharp, size: 30, color: Color(0xFF2D453F)),
                  );
                } else {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
            }
        )
      ),
    );
  }
}
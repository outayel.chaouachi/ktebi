import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BookInfo extends StatelessWidget {
  final String id;
  final String title;
  final String author;
  final String image;
  final String synopsis;
  final String fileurl;
  final String booktype;
  final String bookprice;
  final bool fromLibrary;

  final String _baseUrl = "167.86.121.21:3251";
  BookInfo(this.id, this.title, this.author, this.image, this.synopsis,
      this.fileurl, this.booktype, this.bookprice, [this.fromLibrary = false]);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: InkWell(
        onTap: () async {
          SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setString("bookId", id);
          prefs.setString("bookAuthor", author);
          prefs.setString("bookImage", image);
          prefs.setString("bookTitle", title);
          prefs.setString("bookSynopsis", synopsis);
          prefs.setString("bookUrl", fileurl);
          prefs.setString("bookType", booktype);
          prefs.setString("bookPrice", bookprice);
          if(!fromLibrary)
            {
              Navigator.pushNamed(context, "/bookdetails");
            }
          else {
            Navigator.pushNamed(context, "/bookreading");
          }
        },
        child: Container(


          color: const Color(0xFF2D453F).withOpacity(1),
          padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
          child: Row(
            children: [
              Container(

                decoration: BoxDecoration(
                  color: const Color(0xFFefd1a9),
                  border: Border.all(
                    color: Color(0xFFefd1a9),
                    width: 3.0,

                  ),
                ),
                margin: const EdgeInsets.fromLTRB(10, 10, 30, 20),
                child: Image.network("http://"+ _baseUrl + "/"  + image, width: 120, height: 180),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(child: Text(title,textScaleFactor: 2.5,style:TextStyle(color:Color(0xFFefd1a9))), width:200),
                  const SizedBox(
                    height: 10,
                  ),
                  Text("by author: " + author,style:TextStyle(color:Color(0xFFefd1a9)))
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

class Book {
  final String id;
  final String title;
  final String author;
  final String image;
  final String synopsis;
  final String fileurl;
  final String booktype;
  final String bookprice;


  Book(this.id, this.title, this.author, this.image, this.synopsis, this.fileurl,
      this.booktype,this.bookprice);

  @override
  String toString() {
    return 'Book{title: $title, author: $author, image: $image, synopsis: $synopsis, fileurl: $fileurl, booktype: $booktype, bookprice: $bookprice}';
  }
}
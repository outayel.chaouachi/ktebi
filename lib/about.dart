import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class About extends StatefulWidget {
  const About({Key? key}) : super(key: key);

  @override
  State<About> createState() => _AboutState();
}

class _AboutState extends State<About> {

  final GlobalKey<FormState> _keyForm = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:Text("About ",style:const TextStyle(color:Color(0xFFefd1a9))),
        backgroundColor: const Color(0xFF2D453F),
      ),
      body: Stack(
        children: [
          const Positioned.fill(
            child: Image(
              image: AssetImage('assets/images/background2.png'),
              fit : BoxFit.fill,
            ),
          ),
          Column(
              children: [
                Container(
                    width: double.infinity,
                    margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                    child: Image.asset("assets/images/logo.png", width: 250, height: 250)
                ),
                const Divider(height: 20,thickness: 2,color: Color(0xFFefd1a9),),
                Container(

                  margin: const EdgeInsets.fromLTRB(20, 20, 20, 20),
                  child: Text("Ktébi  is the one app you need to get and enjoy your favourite books.\n \nChoose from a massive collection of popular ebooks, audiobooks \n\nDownload your book to read or listen on the go. Ktébi is free, and optimized for Android devices.",
                      style:const TextStyle(color:Color(0xFFefd1a9),fontWeight: FontWeight.bold),
                      textScaleFactor: 1.5),
                ),
              ]
          ),
        ],
      ),
    );

  }
}
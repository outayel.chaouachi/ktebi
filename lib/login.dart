import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

import 'api/google_signin_api.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {

  late String? _email;
  late String? _password;

  final GlobalKey<FormState> _keyForm = GlobalKey<FormState>();

  final String _baseUrl = "167.86.121.21:3251";
  @override
  void initState() {
    super.initState();
  }

  Future signIn()async{
    final String _baseUrl = "167.86.121.21:3251";
    final user= await GoogleSignInApi.login();
    if (user==null){
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return const AlertDialog(
              title: Text("Information"),
              content: Text("An error occured, please try again !"),
            );
          });
    }else{
      Map<String, dynamic> userData = {
        "Identifiant":user.displayName,
        "Email": user.email,
        "Password" : user.serverAuthCode
      };

      Map<String, String> headers = {
        "Content-Type": "application/json; charset=UTF-8"
      };

      http.post(Uri.http(_baseUrl, "/gsignin"), headers: headers, body: json.encode(userData))
          .then((http.Response response) async {
        if(response.statusCode == 200) {
          Map<String, dynamic> userFromServer = json.decode(response.body);

          SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setString("userId", userFromServer["_id"]);
          prefs.setString("Identifiant", userFromServer["Identifiant"]);

          Navigator.pushNamed(context, "/home");
        }
        else {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return const AlertDialog(
                  title: Text("Information"),
                  content: Text("An error occured, please try again !"),
                );
              });
        }
      });
      //userprefs
      Navigator.pushNamed(context, "/home");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          const Positioned.fill(  //
            child: Image(
              image: AssetImage('assets/images/background2.png'),
              fit : BoxFit.fill,
            ),
          ),
          Form(
            key: _keyForm,
            child: ListView(
                children: [
                  Container(
                      width: double.infinity,
                      margin: const EdgeInsets.fromLTRB(20, 0, 20, 10),
                      child: Image.asset("assets/images/logo.png", width: 311, height: 311)
                  ),
                  Container(
                    margin: const EdgeInsets.fromLTRB(10, 60, 10, 10),
                    child: TextFormField(
                      style:const TextStyle(color: Color(0xFFefd1a9)),
                      decoration: const InputDecoration(
                        /*fillColor: Color(0xFFefd1a9), filled: true,*/
                          labelStyle: TextStyle(
                              color: Color(0xFFefd1a9)
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFefd1a9), width: 1),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFefd1a9), width: 1),
                          ),
                          border: OutlineInputBorder(), labelText: "Email"),
                      onSaved: (String? value) {
                        _email = value;
                      },
                      validator: (String? value) {
                        if(value == null || value.isEmpty) {
                          return "Email shouldn't be empty";
                        }
                        else if(value.length < 5) {
                          return "Email must be at least 5 characters";
                        }
                        else {
                          return null;
                        }
                      },
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),

                    child: TextFormField(
                      obscureText: true,
                      style:const TextStyle(color: Color(0xFFefd1a9)),
                      decoration: const InputDecoration(
                          labelStyle: TextStyle(
                              color: Color(0xFFefd1a9)
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFefd1a9), width: 1),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFefd1a9), width: 1),
                          ),
                          border: OutlineInputBorder(), labelText: "Password"),
                      onSaved: (String? value) {
                        _password = value;
                      },
                      validator: (String? value) {
                        if(value == null || value.isEmpty) {
                          return "Password shouldn't be empty";
                        }
                        else if(value.length < 5) {
                          return "Password must be at least 5 characters";
                        }
                        else {
                          return null;
                        }
                      },
                    ),
                  ),
                  Container(
                      margin: const EdgeInsets.fromLTRB(10, 20, 10, 0),
                      child: ElevatedButton(
                        child: const Text("Sign in", style:TextStyle(color:Color(0xFF2D453F))),
                        style:ElevatedButton.styleFrom(primary:const Color(0xFFefd1a9)),
                        onPressed: () {
                          if(_keyForm.currentState!.validate()) {
                            _keyForm.currentState!.save();

                            Map<String, dynamic> userData = {
                              "email": _email,
                              "password" : _password
                            };

                            Map<String, String> headers = {
                              "Content-Type": "application/json; charset=UTF-8"
                            };

                            http.post(Uri.http(_baseUrl, "/signin"), headers: headers, body: json.encode(userData))
                                .then((http.Response response) async {
                              if(response.statusCode == 200) {
                                Map<String, dynamic> userFromServer = json.decode(response.body);

                                SharedPreferences prefs = await SharedPreferences.getInstance();
                                prefs.setString("userId", userFromServer["_id"]);
                                prefs.setString("Identifiant", userFromServer["Identifiant"]);
                                /*showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return const AlertDialog(
                                        title: Text("Information"),
                                        content: Text("Connexion réussie"),
                                      );
                                    });*/
                                Navigator.pushNamed(context, "/home");
                              }
                              else if(response.statusCode == 401) {
                                showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return const AlertDialog(
                                        title: Text("Information"),
                                        content: Text("Email and/or password incorrect"),
                                      );
                                    });
                              }
                              else {
                                showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return const AlertDialog(
                                        title: Text("Information"),
                                        content: Text("An error happened, please try again !"),
                                      );
                                    });
                              }
                            });
                          }
                        },
                      )
                  ),
                  Container(
                      width: double.infinity,
                      margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: ElevatedButton.icon(
                        icon: FaIcon(
                          FontAwesomeIcons.google,
                          color: Color(0xFF2D453F),
                        ),
                        label: const Text("Sign in via Google", style:TextStyle(color:Color(0xFF2D453F))),
                        style:ElevatedButton.styleFrom(primary:const Color(0xFFefd1a9)),
                        onPressed: signIn,
                      )
                  ),
                  Container(
                      width: double.infinity,
                      margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: ElevatedButton(
                        child: const Text("Sign up", style:TextStyle(color:Color(0xFF2D453F))),
                        style:ElevatedButton.styleFrom(primary:const Color(0xFFefd1a9)),
                        onPressed: () {
                          Navigator.pushNamed(context,"/signup");
                        },
                      )
                  ),
                ]),
          ),

        ],
      ),
    );

  }
}
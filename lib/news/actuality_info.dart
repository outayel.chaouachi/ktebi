import 'package:flutter/material.dart';

class ActualityInfo extends StatelessWidget {
  late String _title;
  late String _description;
  late String _img;
  late String _published_at;


  ActualityInfo(this._title, this._description, this._img,
      this._published_at, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(20),
      child: Material(
        color:  const Color(0xFFefd1a9),
        child: Container(
          margin: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(_title,textScaleFactor: 1.5,style:const TextStyle(color: Colors.black)),
              const SizedBox(height: 20),
              Image.network(_img,),
              const SizedBox(height: 20),
              Text(_description,style:const TextStyle(color: Colors.black)),
              const SizedBox(height: 10),
              Text(_published_at,style:const TextStyle(color: Colors.black)),
            ],
          ),
        ),
      ),
    );
  }
}

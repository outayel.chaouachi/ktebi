import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'actuality.dart';

import 'actuality_info.dart';

class News extends StatefulWidget {
  const News({Key? key}) : super(key: key);

  @override
  _NewsState createState() => _NewsState();
}

class _NewsState extends State<News> {
  final ScrollController _scrollController = ScrollController();

  final String _baseUrl = "newsapi.org";
  final String _path = "/v2/everything";

  late final List<String> _keyWords = ["authors"];

  late int _currentIndex = 0;

  late int year;
  late int month;
  late int day;

  late List<dynamic> newsList;

  late int count;

  late List<Actuality> news = [];
  bool fechedNews = false,fetchingNews = false;

  fetchNews() async{
    if (fechedNews){
      return;
    }
    setState(() {
      fetchingNews = true;
    });


    http.Response response = await http.get(Uri.http(_baseUrl, _path, {
      "q" : _keyWords[_currentIndex],
      "from" : year.toString() + "-" + month.toString() + "-" + day.toString(),
      "sortBy" : "popularity",
      "apiKey" : "58ed7868d88a403aa3b2e484aed69fef"
    }));
    newsList = json.decode(response.body)["articles"];
    print(year.toString() + "-" + month.toString() + "-" + day.toString());
    print(newsList.length);
    print(count);


    List<Actuality> newData = news.length >= 500 ? [] : List.generate(2, (int index) {
      Map<String, dynamic> newFromApi = newsList[index + count];
      count+=2;
      return Actuality(newFromApi["title"], newFromApi["description"], newFromApi["urlToImage"], newFromApi["publishedAt"]);
    });

    if (count >= 20){
      setState(() {
        count = 0;
        _currentIndex++;
        if (_currentIndex > 2){
          _currentIndex = 0;
          day--;
          if (day == 0){
            day = 30;
            month--;
          }
        }
        newsList = json.decode(response.body)["articles"];
      });
    }

    print(newData.length);

    if (newData.isNotEmpty){
      news.addAll(newData);
    }

    setState(() {
      fetchingNews = false;
      fechedNews = newData.isEmpty;
    });

  }

  @override
  void initState() {
    super.initState();
    count = 0;
    year = DateTime.now().year;
    month = DateTime.now().month;
    day = DateTime.now().day;
    fetchNews();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels >= _scrollController.position.maxScrollExtent && !fetchingNews){
        fetchNews();
        print("NEW DATA");
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(

      decoration: const BoxDecoration(
        color: const Color(0xFF2D453F),
        image: DecorationImage(
          image: AssetImage("assets/images/img.png"),
          fit: BoxFit.cover,
        ),
      ),
      child: LayoutBuilder(
          builder: (context, constraints){
            if (news.isNotEmpty){
              return Stack(
                children:[ ListView.separated(
                  controller: _scrollController,
                    itemBuilder: (context, index){
                      return ActualityInfo (news[index].title, news[index].description, news[index].image_url, news[index].publishing_time);
                    },
                    separatorBuilder: (context, index){
                      return const Divider(
                        height: 1,
                      );
                    },
                    itemCount: news.length,
                  ),
                  if(fetchingNews)...[
                    Positioned(
                      left: 0,
                      bottom: 20,
                      child: Container(
                        width: constraints.maxWidth,
                        height: 80,
                        child: const  Center(
                          child: CircularProgressIndicator(color: Colors.blueGrey,),
                        ),
                      ),
                    ),
                  ]
                ]
              );
            }
            else{
              return  Container(
                child: const Center(
                  child: CircularProgressIndicator(),
                ),
              );
            }
          }
      ),
    );
  }
}

class Actuality{
  late String title;
  late String description;
  late String image_url;
  late String publishing_time;

  Actuality(this.title, this.description, this.image_url,
      this.publishing_time);

  @override
  String toString() {
    return 'Actuality{title: $title, description: $description, image_url: $image_url, pblishing_time: $publishing_time}';
  }
}
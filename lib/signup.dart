import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class Signup extends StatefulWidget {
  const Signup({Key? key}) : super(key: key);

  @override
  State<Signup> createState() => _SignupState();
}

class _SignupState extends State<Signup> {
  late String? _identifiant;
  late String? _email;
  late String? _password;

  final GlobalKey<FormState> _keyForm = GlobalKey<FormState>();

  final String _baseUrl = "167.86.121.21:3251";
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          const Positioned.fill(
            child: Image(
              image: AssetImage('assets/images/background2.png'),
              fit : BoxFit.fill,
            ),
          ),
          Form(
            key: _keyForm,
            child: ListView(
                children: [
                  Container(
                      width: double.infinity,
                      margin: const EdgeInsets.fromLTRB(20, 0, 20, 10),
                      child: Image.asset("assets/images/logo.png", width: 311, height: 311)
                  ),
                  Container(
                    margin: const EdgeInsets.fromLTRB(10, 50, 10, 10),
                    child: TextFormField(
                      style:const TextStyle(color: Color(0xFFefd1a9)),
                      decoration: const InputDecoration(
                        /*fillColor: Color(0xFFefd1a9), filled: true,*/
                          labelStyle: TextStyle(
                              color: Color(0xFFefd1a9)
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFefd1a9), width: 1),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFefd1a9), width: 1),
                          ),
                          border: OutlineInputBorder(), labelText: "Username"),
                      onSaved: (String? value) {
                        _identifiant = value;
                      },
                      validator: (String? value) {
                        if(value == null || value.isEmpty) {
                          return "Username shouldn't be empty";
                        }
                        else if(value.length < 5) {
                          return "Username should be at least 5 characters";
                        }
                        else {
                          return null;
                        }
                      },
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                    child: TextFormField(
                      style:const TextStyle(color: Color(0xFFefd1a9)),
                      decoration: const InputDecoration(
                        /*fillColor: Color(0xFFefd1a9), filled: true,*/
                          labelStyle: TextStyle(
                              color: Color(0xFFefd1a9)
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFefd1a9), width: 1),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFefd1a9), width: 1),
                          ),
                          border: OutlineInputBorder(), labelText: "Email"),
                      onSaved: (String? value) {
                        _email = value;
                      },
                      validator: (String? value) {
                        if(value == null || value.isEmpty) {
                          return "Email shouldn't be empty";
                        }
                        else if(value.length < 5) {
                          return "Email must be at least 5 characters";
                        }
                        else {
                          return null;
                        }
                      },
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),

                    child: TextFormField(
                      obscureText: true,
                      style:const TextStyle(color: Color(0xFFefd1a9)),
                      decoration: const InputDecoration(
                          labelStyle: TextStyle(
                              color: Color(0xFFefd1a9)
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFefd1a9), width: 1),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFefd1a9), width: 1),
                          ),
                          border: OutlineInputBorder(), labelText: "Password"),
                      onSaved: (String? value) {
                        _password = value;
                      },
                      validator: (String? value) {
                        if(value == null || value.isEmpty) {
                          return "Password shouldn't be empty";
                        }
                        else if(value.length < 5) {
                          return "Password must be at least 5 characters";
                        }
                        else {
                          return null;
                        }
                      },
                    ),
                  ),
                  Container(
                      margin: const EdgeInsets.fromLTRB(10, 20, 10, 0),
                      child: ElevatedButton(
                        child: const Text("Sign up", style:TextStyle(color:Color(0xFF2D453F))),
                        style:ElevatedButton.styleFrom(primary:const Color(0xFFefd1a9)),
                        onPressed: () {
                          if(_keyForm.currentState!.validate()) {
                            _keyForm.currentState!.save();

                            Map<String, dynamic> userData = {
                              "Identifiant":_identifiant,
                              "Email": _email,
                              "Password" : _password
                            };

                            Map<String, String> headers = {
                              "Content-Type": "application/json; charset=UTF-8"
                            };

                            http.post(Uri.http(_baseUrl, "/register"), headers: headers, body: json.encode(userData))
                                .then((http.Response response) async {
                              if(response.statusCode == 200) {
                                Map<String, dynamic> userFromServer = json.decode(response.body);

                                SharedPreferences prefs = await SharedPreferences.getInstance();
                                showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return const AlertDialog(
                                        title: Text("Information"),
                                        content: Text("Signed up successfully"),
                                      );
                                    });
                                Navigator.pushNamed(context,"/login");

                              }
                              else if(response.statusCode == 401) {
                                showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return const AlertDialog(
                                        title: Text("Information"),
                                        content: Text("Account already exists"),
                                      );
                                    });
                              }
                              else {
                                showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return const AlertDialog(
                                        title: Text("Information"),
                                        content: Text("An error happened, please try again !"),
                                      );
                                    });
                              }
                            });
                          }
                        },
                      )
                  ),
                  Container(
                      width: double.infinity,
                      margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: ElevatedButton(
                        child: const Text("Sign in", style:TextStyle(color:Color(0xFF2D453F))),
                        style:ElevatedButton.styleFrom(primary:const Color(0xFFefd1a9)),
                        onPressed: () {
                          Navigator.pushNamed(context,"/login");
                        },
                      )
                  ),
                ]),
          ),

        ],
      ),
    );

  }
}
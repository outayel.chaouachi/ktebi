import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class ChangePsw extends StatefulWidget {
  const ChangePsw({Key? key}) : super(key: key);

  @override
  State<ChangePsw> createState() => _ChangePswState();
}

class _ChangePswState extends State<ChangePsw> {
  late String? _identifiant;
  late String? _email;
  late String? _password;

  final GlobalKey<FormState> _keyForm = GlobalKey<FormState>();

  final String _baseUrl = "167.86.121.21:3251";
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:Text("Changer de mot passe ",style:const TextStyle(color:Color(0xFFefd1a9))),
        backgroundColor: const Color(0xFF2D453F),
      ),
      body: Stack(
        children: [
          const Positioned.fill(
            child: Image(
              image: AssetImage('assets/images/background2.png'),
              fit : BoxFit.fill,
            ),
          ),
          Form(
            key: _keyForm,
            child: ListView(
                children: [
                  Container(
                      width: double.infinity,
                      margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                      child: Image.asset("assets/images/logo.png", width: 250, height: 250)
                  ),
                  Container(
                    margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                    child: TextFormField(
                      style:const TextStyle(color: Color(0xFFefd1a9)),
                      decoration: const InputDecoration(
                        /*fillColor: Color(0xFFefd1a9), filled: true,*/
                          labelStyle: TextStyle(
                              color: Color(0xFFefd1a9)
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFefd1a9), width: 1),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFefd1a9), width: 1),
                          ),
                          border: OutlineInputBorder(), labelText: "Ancien mot de passe"),
                      onSaved: (String? value) {
                        _identifiant = value;
                      },
                      validator: (String? value) {
                        if(value == null || value.isEmpty) {
                          return "Le mot de passe ne doit pas etre vide";
                        }
                        else if(value.length < 5) {
                          return "Le mot de passe doit avoir au moins 5 caractères";
                        }
                        else {
                          return null;
                        }
                      },
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                    child: TextFormField(
                      style:const TextStyle(color: Color(0xFFefd1a9)),
                      decoration: const InputDecoration(
                        /*fillColor: Color(0xFFefd1a9), filled: true,*/
                          labelStyle: TextStyle(
                              color: Color(0xFFefd1a9)
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFefd1a9), width: 1),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFefd1a9), width: 1),
                          ),
                          border: OutlineInputBorder(), labelText: "Nouveau mot de passe"),
                      onSaved: (String? value) {
                        _email = value;
                      },
                      validator: (String? value) {
                        if(value == null || value.isEmpty) {
                          return "Le mot de passe ne doit pas etre vide";
                        }
                        else if(value.length < 5) {
                          return "le mot de passe doit avoir au moins 5 caractères";
                        }
                        else {
                          return null;
                        }
                      },
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),

                    child: TextFormField(
                      obscureText: true,
                      style:const TextStyle(color: Color(0xFFefd1a9)),
                      decoration: const InputDecoration(
                          labelStyle: TextStyle(
                              color: Color(0xFFefd1a9)
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFefd1a9), width: 1),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFefd1a9), width: 1),
                          ),
                          border: OutlineInputBorder(), labelText: "Confimer mot de passe"),
                      onSaved: (String? value) {
                        _password = value;
                      },
                      validator: (String? value) {
                        if(value == null || value.isEmpty) {
                          return "Le mot de passe ne doit pas etre vide";
                        }
                        else if(value.length < 5) {
                          return "Le mot de passe doit avoir au moins 5 caractères";
                        }
                        else {
                          return null;
                        }
                      },
                    ),
                  ),
                  Container(
                      margin: const EdgeInsets.fromLTRB(10, 20, 10, 0),
                      child: ElevatedButton(
                        child: const Text("Changer mot de passe", style:TextStyle(color:Color(0xFF2D453F))),
                        style:ElevatedButton.styleFrom(primary:const Color(0xFFefd1a9)),
                        onPressed: () {
                          Navigator.pushNamed(context,"/home");
                        },
                      )
                  ),
                ]),
          ),

        ],
      ),
    );

  }
}
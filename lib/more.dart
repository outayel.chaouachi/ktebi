import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:ktebi/api/google_signin_api.dart';
import 'package:shared_preferences/shared_preferences.dart';
class More extends StatelessWidget {
  const More({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(10),
        child : ListView(
          children: [
            const SizedBox(height: 40,),
            Row(
              children: const [
                Icon(
                  Icons.person,
                  color:Color(0xFFefd1a9) ,
                ),
                SizedBox(width: 10,),
                Text("Account",style:TextStyle(fontSize:22,fontWeight:FontWeight.bold, color:Color(0xFFefd1a9)))

              ],
            ),
            const Divider(height: 20,thickness: 1,color: Color(0xFFefd1a9),),
            SizedBox(height: 10,),
            buildAccountOption(context,"Change Password"),
            buildAccountOption(context,"Content Settings"),
            buildAccountOption(context,"Social"),
            buildAccountOption(context,"language "),
            buildAccountOption(context,"Privacy and Security"),
            const SizedBox(height: 40,),
            Row(
              children: const [
                Icon(
                  Icons.restore,
                  color:Color(0xFFefd1a9) ,
                ),
                SizedBox(width: 10,),
                Text("Other",style:TextStyle(fontSize:22,fontWeight:FontWeight.bold, color:Color(0xFFefd1a9)))
              ],
            ),
            const Divider(height: 20,thickness: 1,color: Color(0xFFefd1a9),),
            const SizedBox(height: 10,),
            buildAccountOption(context,"Settings"),
            buildAccountOption(context,"About"),
            buildAccountOption(context,"Logout"),
          ],
        )
    );


  }
  GestureDetector buildAccountOption(BuildContext context,String title){
    return GestureDetector(
      onTap: () async {
        if(title=="Change Password")
        Navigator.pushNamed(context,"/changepsw");
        else if(title=="About")
          Navigator.pushNamed(context,"/about");
        else if(title=="Logout"){
          await GoogleSignInApi.logout();
          SharedPreferences preferences = await SharedPreferences.getInstance();
          await preferences.clear();
          Navigator.pushNamed(context,"/");
        }

        else Navigator.pushNamed(context,"/home");

      },
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8,horizontal: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween ,
          children: [

            Text(title,style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w500,
                color: Colors.white
            ),),
            Icon(Icons.arrow_forward_ios,color: Color(0xFFefd1a9),),
          ],
        ),
      ),
    );
  }
}

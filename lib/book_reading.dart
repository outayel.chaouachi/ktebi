import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'package:dio/dio.dart';
import 'package:epub_view/epub_view.dart' hide Image;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:path_provider/path_provider.dart';
import 'package:http/http.dart';
import 'dart:convert';

import 'audioplayerwidget.dart';

class BookReading extends StatefulWidget {
  const BookReading({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _BookReadingState();
  }
}



class _BookReadingState extends State<BookReading> {
  late String id;
  late String title;
  late String author;
  late String image;
  late String synopsis;
  late String fileurl;
  late String booktype;
  late String userID;
  late SharedPreferences prefs;
  late Future<bool> bookInfo;
  bool loading = false;
  final String _baseUrl = "167.86.121.21:3251";
  late EpubController _epubReaderController;
  Dio dio = Dio();
  late Future<EpubBook> epubbookDD;




  download() async {
    await downloadFile();
    print("File downloaded");
  }

  Future downloadFile() async {
    if (await Permission.storage.isGranted) {
      await Permission.storage.request();
      await startDownload();
    } else {
      await startDownload();
    }
   // await startDownload();
  }

  startDownload() async {
    Directory? appDocDir = Platform.isAndroid
        ? await getExternalStorageDirectory()
        : await getApplicationDocumentsDirectory();

    String path = appDocDir!.path + '/'+title+(booktype == "EBook" ? '.epub' : '.mp3');
    File file = File(path);
//    await file.delete();
    print("http://"+_baseUrl + "/" + fileurl);
    if (!File(path).existsSync()) {
      print("downloading because file not existing in local");
      await file.create();

        await dio.download(
          "http://"+_baseUrl + "/" + fileurl,
          path,
          deleteOnError: true,
          onReceiveProgress: (receivedBytes, totalBytes) {
            print((receivedBytes / totalBytes * 100).toStringAsFixed(0));
            //Check if download is complete and close the alert dialog
            if (receivedBytes == totalBytes) {
              print("Finished donwload");
              loading = false;
              //setState(() {});
            }
          },
        );
    } else {
      print("file exists");
      loading = false;
      //setState(() {});
    }
  }

  Future<Uint8List> _loadFromAssets() async {
    Directory? appDocDir = Platform.isAndroid
        ? await getExternalStorageDirectory()
        : await getApplicationDocumentsDirectory();

    String path = appDocDir!.path + '/'+title+'.epub';

    await download();

    /*final bytes = await rootBundle.load('assets/'+title+'.epub').catchError( (e) {
        print(e);
    });*/
    final bytes = File(path).readAsBytes();

    print("loaded bytes");
    return bytes;
  }

  Future<Uint8List> _loadFromAssetsAudio() async {
    Directory? appDocDir = Platform.isAndroid
        ? await getExternalStorageDirectory()
        : await getApplicationDocumentsDirectory();

    String path = appDocDir!.path + '/'+title+'.mp3';

    await download();

    /*final bytes = await rootBundle.load('assets/'+title+'.epub').catchError( (e) {
        print(e);
    });*/
    final bytes = File(path).readAsBytes();

    print("loaded bytes");
    return bytes;
  }

  Future<Widget> correspondingAction() async
  {
      /*await getBookInfo();
      setState((){
        bookInfo = getBookInfo();
      });
      await getBookInfo();*/
      if(booktype == "EBook")
        {
          return FutureBuilder<EpubBook>(
            future: epubbookData(),
            builder: (_, snapshot) {
              if (snapshot.hasData) {
                _epubReaderController = EpubController(
                  document: epubbookData(),
                );
                return EpubView(
                  controller: _epubReaderController,
                );

              }

              else {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
            },
          );
        } else {
        await _loadFromAssetsAudio();
        Directory? appDocDir = Platform.isAndroid
            ? await getExternalStorageDirectory()
            : await getApplicationDocumentsDirectory();

        String path = appDocDir!.path + '/'+title+'.mp3';
        print(path);
          return Column(
            children: [
              Container(
                decoration: BoxDecoration(
                  color: const Color(0xFFefd1a9),
                  border: Border.all(
                    color: Color(0xFF2D453F),
                    width: 5.0,

                  ),
                ),
                  margin: const EdgeInsets.fromLTRB(0, 50, 0, 0),
                  child: Image.network("http://"+ _baseUrl + "/"  + image, width: 160, height: 160),
              ),
              Container(
                margin: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                child: Align(
                  alignment: Alignment.center,
                  child: Text(
                    title,
                    textScaleFactor: 2,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color:Color(0xFF2D453F),
                      fontWeight: FontWeight. bold,
                    ),
                  ),
                ),
              ),
              AudioPlayerWidget(url: path),
            ],
          );
      }
  }

  Future<bool> getBookInfo() async {
    prefs = await SharedPreferences.getInstance();
    userID = prefs.getString("userId")!;
    id = prefs.getString("bookId")!;
    author = prefs.getString("bookAuthor")!;
    image = prefs.getString("bookImage")!;
    title = prefs.getString("bookTitle")!;
    synopsis = prefs.getString("bookSynopsis")!;
    fileurl = prefs.getString("bookUrl")!;
    booktype = prefs.getString("bookType")!;

    return true;
  }
  
  Future<EpubBook> epubbookData()
  {
    return _loadFromAssets().then(EpubReader.readBook);
  }

  @override
  void initState() {
    bookInfo = getBookInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor:Color(0xFFefd1a9),
        appBar: AppBar(
        title: Text("Book Reader", style:const TextStyle(color:Color(0xFFefd1a9))),

      backgroundColor: const Color(0xFF2D453F),
      ),
    body: FutureBuilder<bool>(

      future:getBookInfo(),
      builder: (context, snapshot) {
        if(snapshot.hasData)
          {
            return FutureBuilder<Widget>(
                future:correspondingAction(),
                builder: (context, snapshot) {
                  if(snapshot.hasData) {
                    return snapshot.data!;
                  } else {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                }
            );
          } else {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
      }
    )
    );
  }
}
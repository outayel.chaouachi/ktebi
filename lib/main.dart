import 'package:flutter/material.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:ktebi/changepsw.dart';
import 'package:ktebi/comments.dart';
import 'package:ktebi/home.dart';

import 'Splash.dart';
import 'about.dart';
import 'book_details.dart';
import 'book_reading.dart';
import 'login.dart';
import 'signup.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  Stripe.publishableKey = "pk_test_51LAgX0COqyso6xw7RCm5JwB2hqxnw9hcB4lQIz4G6x73zhRKi9j0UnB4AbXYTVbMaVqNszOLBOGBMIW9WqFVzi7S00yvBXsrWi";
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'KTEBI',
      routes: {
        "/": (BuildContext context) {
          return const Splash();
        },
        "/login": (BuildContext context) {
          return const Login();
        },
        "/signup": (BuildContext context) {
          return const Signup();
        },
        "/home": (BuildContext context) {
          return const Home();
        },
        "/bookdetails": (BuildContext context) {
          return BookDetails();
        },
        "/bookreading": (BuildContext context) {
          return BookReading();
        },
        "/comments": (BuildContext context) {
          return Comments();
        },
        "/changepsw": (BuildContext context) {
          return ChangePsw();
        },
        "/about": (BuildContext context) {
          return About();
        },
      },
    );
  }
}

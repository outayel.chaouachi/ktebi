import 'dart:async';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'book_info.dart';

class Library extends StatefulWidget {
  const Library({Key? key}) : super(key: key);

  @override
  _LibraryState createState() => _LibraryState();
}

class _LibraryState extends State<Library> {
  late Future<bool> fetchedBooks;

  late List<Book> _books = [];
  late List<Book> _booksTest = [];

  final String _baseUrl = "167.86.121.21:3251";

  Future<bool> fetchBooks() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Map<String, dynamic> userData = {
      "_id": prefs.getString("userId")!,
    };

    Map<String, String> headers = {
      "Content-Type": "application/json; charset=UTF-8"
    };

    http.post(Uri.http(_baseUrl, "/fetchMyBooks"), headers: headers, body: json.encode(userData))
        .then((http.Response response) async {
      if(response.statusCode == 200) {
        var booksFromServer = json.decode(response.body);
        for(int i = 0; i < booksFromServer.length; i++) {
          Map<String, dynamic> bookFromServer = booksFromServer[i];
          print(bookFromServer["fileUrl"].toString());
          _books.add(Book(bookFromServer["_id"].toString(), bookFromServer["Title"].toString(), bookFromServer["Author"].toString(),
              bookFromServer["Image"].toString(), bookFromServer["Synopsis"].toString(),bookFromServer["fileUrl"].toString(),bookFromServer["bookType"].toString(),bookFromServer["bookPrice"].toString()));
        }
        setState(() => _booksTest = _books);
      }
    });

    return true;
  }

  @override
  void initState() {
    fetchedBooks = fetchBooks();
    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    return  FutureBuilder(
      future: fetchedBooks,
      builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
        if(snapshot.hasData) {
          return ListView.builder(
            itemCount: _booksTest.length,
            itemBuilder: (BuildContext context,int index) {
              return BookInfo(_booksTest[index].id, _booksTest[index].title, _booksTest[index].author, _booksTest[index].image,
                  _booksTest[index].synopsis, _booksTest[index].fileurl, _booksTest[index].booktype, _booksTest[index].bookprice,true);
            },
          );
        }
        else {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}
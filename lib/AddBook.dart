import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:async/async.dart';
import 'dart:convert';
import 'package:file_picker/file_picker.dart';
import 'package:path/path.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum BookType { EBook, AudioBook }

class AddBook extends StatefulWidget {
  const AddBook({Key? key}) : super(key: key);

  @override
  _AddBookState createState() => _AddBookState();
}

class _AddBookState extends State<AddBook> {

  final String _baseUrl = "167.86.121.21:3251";
  final GlobalKey<FormState> _keyForm = GlobalKey<FormState>();

  late String? bookTitle;
  late String? bookSynopsis;
  late String? bookCharacters;
  late String? Identifiant;
  late String? bookPrice;

  BookType _bookType = BookType.EBook;

  /*Future<bool> fetchBooks() async {
    http.Response response = await http.get(Uri.http(_baseUrl, "/fetchBooks"));

    List<dynamic> booksFromServer = json.decode(response.body);

    for(int i = 0; i < booksFromServer.length; i++) {
      Map<String, dynamic> bookFromServer = booksFromServer[i];
      _books.add(Book(bookFromServer["_id"], bookFromServer["Title"], bookFromServer["Author"],
          bookFromServer["Image"], bookFromServer["Synopsis"],bookFromServer["fileUrl"],bookFromServer["bookType"]));
    }

    return true;
  }*/

  String? _fileName;
  PlatformFile? _pathImage;
  PlatformFile? _pathBook;
  String? _directoryPath;
  String? _extension = "epub";
  String? _extension2 = "wav, mp3, flac";
  bool _loadingPath = false;
  bool _multiPick = false;
  TextEditingController _controller = TextEditingController();
  late Future<PlatformFile?> pickedFile = Future.value(null);

  Future<PlatformFile?> assignImage() async {

    return _pathImage;
  }

  void _openFileExplorer(bool image) async {
    setState(() => _loadingPath = true);
    try {
      _directoryPath = null;
      image ? (_pathImage = (await FilePicker.platform.pickFiles(
        type: FileType.image,
        allowMultiple: false,
      ))
          ?.files[0])
          :
      (_pathBook = (await FilePicker.platform.pickFiles(
        type: FileType.custom,
        allowMultiple: false,
        allowedExtensions: (_bookType == BookType.EBook ? ((_extension?.isNotEmpty ?? false)
            ? _extension?.replaceAll(' ', '').split(',')
            : null)
            :
        ((_extension2?.isNotEmpty ?? false)
            ? _extension2?.replaceAll(' ', '').split(',')
            : null)

        )
      ))
          ?.files[0]);
    } on PlatformException catch (e) {
      print("Unsupported operation" + e.toString());
    } catch (ex) {
      print(ex);
    }
    if (!mounted) return;
    setState(() {
      _loadingPath = false;
      if(image)
        {
          pickedFile = assignImage();
        }
      print(image ? _pathImage!.extension : _pathBook!.extension);
      _fileName =
      image ? (_pathImage != null ? _pathImage!.name.toString() : '...') : (_pathBook != null ? _pathBook!.name.toString() : '...');
    });
  }

  void GetIdentifier() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Identifiant = prefs.getString("Identifiant");
  }

  void SendRequest(context) async {
    var req = http.MultipartRequest('POST', Uri.parse("http://"+_baseUrl + "/addBook"));
    req.fields["Title"] = bookTitle!;
    req.fields["Author"] = Identifiant!;
    req.fields["Synopsis"] = bookSynopsis!;
    req.fields["Characters"] = bookCharacters!;
    req.fields["bookType"] = _bookType == BookType.EBook ? "EBook" : "AudioBook";
    req.fields["bookPrice"] = bookPrice!;
    File imageFile = File(_pathImage!.path.toString());
    var imageStream = new http.ByteStream(DelegatingStream.typed(imageFile.openRead()));
    var imageLength = await imageFile.length();
    var multipartImageFile = new http.MultipartFile('bookFiles', imageStream, imageLength, filename: basename(imageFile.path));
    File bookFile = File(_pathBook!.path.toString());
    var bookStream = new http.ByteStream(DelegatingStream.typed(bookFile.openRead()));
    var bookLength = await bookFile.length();
    var multipartBookFile = new http.MultipartFile('bookFiles', bookStream, bookLength, filename: basename(imageFile.path));
    List<http.MultipartFile> fileList = <http.MultipartFile>[];
    fileList.add(multipartImageFile);
    fileList.add(multipartBookFile);
    req.files.addAll(fileList);
    var response = await req.send();
    response.stream.transform(utf8.decoder).listen((value) {
      print(value);
    });
    if(response.statusCode == 201)
      {
        showDialog(
            context: context,
            builder: (BuildContext context2) {
              return AlertDialog(
                title: Text("Add Book"),
                content: Text("Book added successfully"),
                  actions: [
                  TextButton(
                  onPressed: () => Navigator.pushNamed(context,"/home"),
                child: Text('OK'),
              )]
              );
            });
        ;
      }


  }

  @override
  void initState() {
    //fetchedBooks = fetchBooks();
    GetIdentifier();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          const Positioned.fill(
            child: Image(
              image: AssetImage('assets/images/background2.png'),
              fit : BoxFit.fill,
            ),
          ),
          Form(
            key: _keyForm,
            child: ListView(
                children: [
                  FutureBuilder<PlatformFile?>(
                  future: pickedFile,
                      builder: (context, snap) {
                        if(snap.hasData) {
                          return Column(
                            children: [Container(
                                margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                child: Image(image:FileImage(
                                    File(_pathImage!.path!),
                                    scale: 1
                                ),width: 460, height: 215,),
                            ),
                              Container(
                                margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Flexible(
                                      child: ListTile(
                                        title: const Text('EBook', style: TextStyle(color: Color(0xFFefd1a9))),
                                        leading: Radio<BookType>(
                                          fillColor: MaterialStateColor.resolveWith((states) => Color(0xFFefd1a9)),
                                          value: BookType.EBook,
                                          groupValue: _bookType,
                                          onChanged: (value) {
                                            setState(() {
                                              _bookType = value!;
                                              print(_bookType);
                                            });
                                          },
                                        ),
                                      ),
                                    ),
                                    Flexible(
                                      child: ListTile(
                                        title: const Text('AudioBook', style: TextStyle(color: Color(0xFFefd1a9))),
                                        leading: Radio<BookType>(
                                          fillColor: MaterialStateColor.resolveWith((states) => Color(0xFFefd1a9)),
                                          value: BookType.AudioBook,
                                          groupValue: _bookType,
                                          onChanged: (value) {
                                            setState(() {
                                              _bookType = value!;
                                              print(_bookType);
                                            });
                                          },
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                                ],
                          );
                        } else {
                            return                   Container(
                              margin: EdgeInsets.fromLTRB(0, 50, 0, 10),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Flexible(
                                    child: ListTile(
                                      title: const Text('EBook', style: TextStyle(color: Color(0xFFefd1a9))),
                                      leading: Radio<BookType>(
                                        fillColor: MaterialStateColor.resolveWith((states) => Color(0xFFefd1a9)),
                                        value: BookType.EBook,
                                        groupValue: _bookType,
                                        onChanged: (value) {
                                          setState(() {
                                            _bookType = value!;
                                            print(_bookType);
                                          });
                                        },
                                      ),
                                    ),
                                  ),
                                  Flexible(
                                    child: ListTile(
                                      title: const Text('AudioBook', style: TextStyle(color: Color(0xFFefd1a9))),
                                      leading: Radio<BookType>(
                                        fillColor: MaterialStateColor.resolveWith((states) => Color(0xFFefd1a9)),
                                        value: BookType.AudioBook,
                                        groupValue: _bookType,
                                        onChanged: (value) {
                                          setState(() {
                                            _bookType = value!;
                                            print(_bookType);
                                          });
                                        },
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            );
                        }
                      }
                  ),
                  Container(
                    margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                    child: TextFormField(
                      style:const TextStyle(color: Color(0xFFefd1a9)),
                      decoration: const InputDecoration(
                        /*fillColor: Color(0xFFefd1a9), filled: true,*/
                          labelStyle: TextStyle(
                              color: Color(0xFFefd1a9)
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFefd1a9), width: 1),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFefd1a9), width: 1),
                          ),
                          border: OutlineInputBorder(), labelText: "Title"),
                      onSaved: (String? value) {
                        bookTitle = value;
                      },
                      validator: (String? value) {
                        if(value == null || value.isEmpty) {
                          return "Title shouldn't be empty";
                        }
                        else {
                          return null;
                        }
                      },
                    ),
                  ),

                  Container(
                    margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                    child: TextFormField(
                      keyboardType: TextInputType.multiline,
                      maxLines: null,
                      obscureText: false,
                      style:const TextStyle(color: Color(0xFFefd1a9)),
                      decoration: const InputDecoration(
                          labelStyle: TextStyle(
                              color: Color(0xFFefd1a9)
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFefd1a9), width: 1),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFefd1a9), width: 1),
                          ),
                          border: OutlineInputBorder(), labelText: "Synopsis"),
                      onSaved: (String? value) {
                        bookSynopsis = value;
                      },
                      validator: (String? value) {
                        if(value == null || value.isEmpty) {
                          return "Synopsis shouldn't be empty";
                        }
                      },
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                    child: TextFormField(
                      style:const TextStyle(color: Color(0xFFefd1a9)),
                      decoration: const InputDecoration(
                        /*fillColor: Color(0xFFefd1a9), filled: true,*/
                          labelStyle: TextStyle(
                              color: Color(0xFFefd1a9)
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFefd1a9), width: 1),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFefd1a9), width: 1),
                          ),
                          border: OutlineInputBorder(), labelText: "Characters"),
                      onSaved: (String? value) {
                        bookCharacters = value;
                      },
                      validator: (String? value) {
                        if(value == null || value.isEmpty) {
                          return "Character list shouldn't be empty";
                        }
                      },
                    ),
                  ),
                  Container(
                      margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                      child: TextFormField(
                      keyboardType: TextInputType.number,
                      style:const TextStyle(color: Color(0xFFefd1a9)),
                      decoration: const InputDecoration(
                      /*fillColor: Color(0xFFefd1a9), filled: true,*/
                      labelStyle: TextStyle(
                      color: Color(0xFFefd1a9)
                      ),
                      focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xFFefd1a9), width: 1),
                      ),
                      enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xFFefd1a9), width: 1),
                      ),
                      border: OutlineInputBorder(), labelText: "Price (Dollars)"),
                      onSaved: (String? value) {
                      bookPrice = value;
                      },
                      validator: (String? value) {
                      if(value == null || value.isEmpty || int.parse(value) < 0) {
                      return "Book Price shouldn't be empty";
                      }
                      else {
                      return null;
                      }
                      },
                      ),
                  )
                  ,
                  Container(
                      margin: const EdgeInsets.fromLTRB(10, 20, 10, 0),
                      child: Row(
                        children: [
                          ElevatedButton(
                            child: const Text("Choose cover", style:TextStyle(color:Color(0xFF2D453F))),
                            style:ElevatedButton.styleFrom(primary:const Color(0xFFefd1a9)),
                            onPressed: () {
                              _openFileExplorer(true);
                              /*var multipartFile = new http.MultipartFile('file', stream, length,
                                    filename: basename(imageFile.path));
                                //contentType: new MediaType('image', 'png'));

                                request.files.add(multipartFile);*/
                            },
                          ),
                          SizedBox(width: 135),
                          ElevatedButton(
                            child: const Text("Choose book file", style:TextStyle(color:Color(0xFF2D453F))),
                            style:ElevatedButton.styleFrom(primary:const Color(0xFFefd1a9)),
                            onPressed: () {
                              _openFileExplorer(false);
                            },
                          ),
                        ],
                      )
                  ),
                  Container(
                      margin: const EdgeInsets.fromLTRB(10, 20, 10, 0),
                      child: ElevatedButton(
                        child: const Text("Add", style:TextStyle(color:Color(0xFF2D453F))),
                        style:ElevatedButton.styleFrom(primary:const Color(0xFFefd1a9)),
                        onPressed: () {
                          if(_keyForm.currentState!.validate() && _pathBook != null && _pathImage != null) {
                            _keyForm.currentState!.save();
                            print("file paths: ");
                            print(_pathImage!.name.toString());
                            print(_pathBook!.name.toString());
                            print(bookTitle);
                            print(bookSynopsis);
                            print(bookCharacters);
                            print(_bookType);

                            SendRequest(context);

                          }
                        },
                      )
                  ),
                ]),
          ),
        ],
      ),
    );
  }
}
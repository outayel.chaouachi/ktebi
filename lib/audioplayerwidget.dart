import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';

class AudioPlayerWidget extends StatefulWidget {
  final String url;
  final bool isAsset;
  const AudioPlayerWidget({
    Key? key,
    required this.url,
    this.isAsset = false,
  }) : super(key: key);
  @override
  _AudioPlayerWidgetState createState() => _AudioPlayerWidgetState();
}

class _AudioPlayerWidgetState extends State<AudioPlayerWidget> {
  late AudioPlayer _audioPlayer;
  late AudioCache _audioCache;
  PlayerState _playerState = PlayerState.STOPPED;
  bool get _isPlaying => _playerState == PlayerState.PLAYING;
  bool get _isLocal => !widget.url.contains('https');
  double _currentSliderValue = 0;
  late Future<int> fileDuration;
  late Duration fDuration;

  @override
  void initState() {

    _audioPlayer = AudioPlayer(mode: PlayerMode.MEDIA_PLAYER);
    _audioCache = AudioCache(fixedPlayer: _audioPlayer);
    _audioPlayer.onPlayerError.listen((msg) {
      print('audioPlayer error : $msg');
      setState(() {
        _playerState = PlayerState.STOPPED;
      });
    });

    _audioPlayer.onAudioPositionChanged.listen((p) => {
      print("Audio Position changing " + p.inMilliseconds.toString()),
      setState(() {
        _currentSliderValue = p.inMilliseconds.toDouble();
      })
    });

    super.initState();
  }

  Future<bool> getFileDuration() async
  {
    await _audioPlayer.setUrl(widget.url);
    fDuration = Duration(milliseconds: await _audioPlayer.getDuration());
    return true;
  }

  @override
  void dispose() {
    _audioPlayer.dispose();
    super.dispose();
  }

  _playPause() async {
    if (_playerState == PlayerState.PLAYING) {
      final playerResult = await _audioPlayer.pause();
      if (playerResult == 1) {
        setState(() {
          _playerState = PlayerState.PAUSED;
        });
      }
    } else if (_playerState == PlayerState.PAUSED) {
      final playerResult = await _audioPlayer.resume();
      if (playerResult == 1) {
        setState(() {
          _playerState = PlayerState.PLAYING;
        });
      }
    } else {
      if (widget.isAsset) {
        _audioPlayer = await _audioCache.play(widget.url);
        setState(() {
          _playerState = PlayerState.PLAYING;
        });
      } else {
        final playerResult = await _audioPlayer.play(widget.url, isLocal: _isLocal);
        if (playerResult == 1) {
          setState(() {
            _playerState = PlayerState.PLAYING;
          });
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<bool>(
      future:getFileDuration(),
      builder:(context,snapshot)
        {
          if(snapshot.hasData)
            {
              return Column(
                  children:[
                    Container(
                        color: const Color(0xFF2D453F).withOpacity(1),
                        margin: const EdgeInsets.fromLTRB(0, 50, 0, 0),
                        child: Slider(
                          activeColor: Color(0xFFefd1a9),
                          inactiveColor: Color(0xFFefd1a9),
                          thumbColor: Color(0xFFefd1a9),
                          value: _currentSliderValue,
                          min: 0,
                          max: fDuration.inMilliseconds.toDouble(),
                          divisions: null,
                          label: ((_currentSliderValue / fDuration.inMilliseconds) * 100).toString() + "%",
                          onChanged: (double value) {
                            setState(() {
                              _currentSliderValue = value;
                              _audioPlayer.seek(Duration(milliseconds:value.toInt()));
                            });
                          },
                        )
                    ),
                    Container(
                        margin: const EdgeInsets.fromLTRB(175, 30, 0, 20),
                        child:
                        Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children:[
                              ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  primary: Color(0xFF2D453F).withOpacity(1), // Background color
                                  onPrimary: Color(0xFFefd1a9), // Text Color (Foreground color)
                                ),
                                onPressed: () {
                                  _playPause();
                                },
                                child: const Icon(Icons.play_arrow_rounded),
                              )
                            ]
                        )
                    ),
                  ]
              );
            } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        }
    );
  }
}
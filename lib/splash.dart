import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ktebi/api/google_signin_api.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class Splash extends StatefulWidget {
  const Splash({Key? key}) : super(key: key);

  @override
  State<Splash> createState() => _SplashState();
}

class _SplashState extends State<Splash> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        const Positioned.fill(  //
          child: Image(
            image: AssetImage('assets/images/background.png'),
            fit : BoxFit.fill,
          ),
        ),

        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children:[
            Container(
                width: double.infinity,
                margin: const EdgeInsets.fromLTRB(20, 0, 20, 10),
                child: Image.asset("assets/images/logo.png", width: 311, height: 311)
            ),
            Container(
                width: double.infinity,
                margin: const EdgeInsets.fromLTRB(10, 200, 10, 0),
                child: ElevatedButton(
                  child: const Text("Sign in", style:TextStyle(color:Color(0xFF2D453F))),
                  style:ElevatedButton.styleFrom(primary:const Color(0xFFefd1a9)),
                  onPressed: () {
                      Navigator.pushNamed(context,"/login");
                  },
                )
            ),
            Container(
                width: double.infinity,
                margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: ElevatedButton(
                  child: const Text("Sign up", style:TextStyle(color:Color(0xFF2D453F))),
                  style:ElevatedButton.styleFrom(primary:const Color(0xFFefd1a9)),
                  onPressed: () {
                    Navigator.pushNamed(context,"/signup");
                  },
                )
            ),
            Container(
                width: double.infinity,
                margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: ElevatedButton.icon(
                  icon: FaIcon(
                    FontAwesomeIcons.google,
                    color: Color(0xFF2D453F),
                  ),
                  label: const Text("Sign up via Google", style:TextStyle(color:Color(0xFF2D453F))),
                  style:ElevatedButton.styleFrom(primary:const Color(0xFFefd1a9)),
                  onPressed: signIn,
                )
            ),
          ]
        )

      ],
    );

  }
  Future signIn()async{
    final String _baseUrl = "167.86.121.21:3251";
    final user= await GoogleSignInApi.login();
    if (user==null){
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return const AlertDialog(
              title: Text("Information"),
              content: Text("Une erreur s'est produite. Veuillez réessayer !"),
            );
          });
    }else{
      Map<String, dynamic> userData = {
        "Identifiant":user.displayName,
        "Email": user.email,
        "Password" : user.serverAuthCode
      };

      Map<String, String> headers = {
        "Content-Type": "application/json; charset=UTF-8"
      };

      http.post(Uri.http(_baseUrl, "/gsignin"), headers: headers, body: json.encode(userData))
          .then((http.Response response) async {
        if(response.statusCode == 200) {
          Map<String, dynamic> userFromServer = json.decode(response.body);

          SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setString("userId", userFromServer["_id"]);
          prefs.setString("Identifiant", userFromServer["Identifiant"]);

          Navigator.pushNamed(context, "/home");
        }
        else {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return const AlertDialog(
                  title: Text("Information"),
                  content: Text("Une erreur s'est produite. Veuillez réessayer !"),
                );
              });
        }
      });
      //userprefs
      Navigator.pushNamed(context, "/home");
    }
  }
}